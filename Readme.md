# Übung
* Markus Holzer
* se13m007
* se13m007@technikum-wien.at

# Android Projekt:
* Input-Textfeld für einen Usernamen
* Senden-Button
* Beim Klick auf "Senden" werden die Repos in einer Liste angezeigt
* Beim Klick auf ein Repo werden die Contributors davon in einer Liste angezeigt
* Beim Klick auf einen Contributor werden die Repos von diesem User angezeigt
* Beim Klick im Menü auf "Output (nicht) anzeigen" werden diese Vorgänge nicht durchgeführt
* Beim erneuten Klick auf den Menübutton wird es wieder durchgeführt
* Mögliche Fehler (z.B. User existiert nicht) werden abgefangen und in einem Dialog angezeigt
