package at.wien.technikum.holzer.se13m007_helloworld;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Holzer on 25.11.2014.
 * MSE 3 B
 */
public class MainFragment extends Fragment {
    /**
     * is being called when the fragment is created
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // get the visual elements -> view, button, input-textfield, output-textview
        View rootView = inflater.inflate(R.layout.activity_main, container, false);
        final Button button = (Button) rootView.findViewById(R.id.button_send);
        final EditText input = (EditText) rootView.findViewById(R.id.username);
        final TextView output = (TextView) rootView.findViewById(R.id.output_message);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // create a toast if the 'send'-button is clicked
                Toast.makeText(getActivity().getApplicationContext(), "Send...", Toast.LENGTH_LONG).show();
                // there is a menu-button to enable/disable showing the output
                if (MainActivity.SHOW_OUTPUT) {
                    final String inputText = input.getText().toString();
                    output.setText(inputText);

                    RepoFragment repoFragment = new RepoFragment();
                    repoFragment.setUsername(inputText);
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.container, repoFragment);
                    fragmentTransaction.addToBackStack(null).commit();
                }
            }
        });
        return rootView;
    }
}
