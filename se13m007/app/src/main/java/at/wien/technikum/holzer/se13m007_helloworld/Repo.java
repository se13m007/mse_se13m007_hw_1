package at.wien.technikum.holzer.se13m007_helloworld;

/**
 * Created by Holzer on 25.11.2014.
 * MSE 3 B
 */
public class Repo {
    public String name;
    public String language;

    public Repo(String name, String language) {
        this.name = name;
        this.language = language;
    }

    @Override
    public String toString() {
        return "Repo [name=" + name + ", language=" + language + "]";
    }
}
