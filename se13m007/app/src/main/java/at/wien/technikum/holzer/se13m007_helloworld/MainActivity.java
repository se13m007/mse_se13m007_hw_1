package at.wien.technikum.holzer.se13m007_helloworld;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Holzer on 21.11.2014.
 * MSE 3 B
 */
public class MainActivity extends Activity {
    public static boolean SHOW_OUTPUT = true;

    /**
     * is being called when the fragment is created
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // add fragment to contentView
        setContentView(R.layout.fragment_main);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new MainFragment())
                    .commit();
        }
    }

    /**
     * is being called when the activity will resume (from minimizing or something else)
     * checks if there is an internet connection
     */
    @Override
    protected void onResume() {
        super.onResume();
        if (!isOnline()) {
            // if no internet connection -> show FailureMessage in dialog
            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage(R.string.network_failure_message)
                    .setTitle(R.string.network_failure_title)
                    .setCancelable(false)
                    .setPositiveButton("Ok, move on", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // just close the dialog box and disable send-button
                            dialog.cancel();
                            Button button = (Button) findViewById(R.id.button_send);
                            button.setEnabled(false);
                        }
                    })
                    .setNegativeButton("Close App", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // close current activity
                            MainActivity.this.finish();
                        }
                    });
            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    /**
     * checks if there is an internet connection - just for internal class purposes -> private
     */
    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /**
     * creates options menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * is being called if an options item is being selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_input:
                SHOW_OUTPUT = !SHOW_OUTPUT;
                ((TextView) findViewById(R.id.output_message)).setText("");
                if (SHOW_OUTPUT) {
                    item.setTitle(R.string.dont_show_output);
                } else {
                    item.setTitle(R.string.show_output);
                }
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
