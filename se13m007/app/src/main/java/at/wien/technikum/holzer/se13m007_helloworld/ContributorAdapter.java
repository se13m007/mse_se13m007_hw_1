package at.wien.technikum.holzer.se13m007_helloworld;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Holzer on 10.12.2014.
 * MSE 3 B
 */
public class ContributorAdapter extends ArrayAdapter<Contributor> {
    public ContributorAdapter(Context context, List<Contributor> contributorList) {
        super(context, 0, contributorList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Contributor contributor = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_contributor, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvContributions = (TextView) convertView.findViewById(R.id.tvContributions);
        // Populate the data into the template view using the data object
        tvName.setText(contributor.login);
        tvContributions.setText(contributor.contributions + " contributions");
        // Return the completed view to render on screen
        return convertView;
    }
}
