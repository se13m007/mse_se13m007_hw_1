package at.wien.technikum.holzer.se13m007_helloworld;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Holzer on 10.12.2014.
 * MSE 3 B
 */
public class RepoAdapter extends ArrayAdapter<Repo> {
    public RepoAdapter(Context context, List<Repo> repoList) {
        super(context, 0, repoList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Repo repo = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_repo, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
        TextView tvLanguage = (TextView) convertView.findViewById(R.id.tvLanguage);
        // Populate the data into the template view using the data object
        tvName.setText(repo.name);
        tvLanguage.setText(repo.language);
        // Return the completed view to render on screen
        return convertView;
    }
}
