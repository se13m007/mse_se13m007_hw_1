package at.wien.technikum.holzer.se13m007_helloworld;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Holzer on 02.12.2014.
 * MSE 3 B
 */
public class RetrofitApiClient {
    private static RetrofitApiClient instance;
    private GitHubService service;

    private RetrofitApiClient(GitHubService service) {
        this.service = service;
    }

    /**
     * creates an instance the first time the apiclient is called
     * accesses the endpoint of api.github.com
     */
    public static RetrofitApiClient getInstance() {
        if (instance == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint("https://api.github.com")
                    .build();

            GitHubService service = restAdapter.create(GitHubService.class);
            instance = new RetrofitApiClient(service);
        }
        return instance;
    }

    /**
     * get all repos for the provided user
     */
    public void getReposForUser(String user, Callback<List<Repo>> callback) {
        service.listReposAsync(user, callback);
    }

    /**
     * get all contributors of the repo for the provided user
     */
    public void getContributorsForUserRepo(String user, String repo, Callback<List<Contributor>> callback) {
        service.listContributorsAsync(user, repo, callback);
    }

    /**
     * the interface for the GitHubService
     */
    public interface GitHubService {
        @GET("/users/{user}/repos")
        void listReposAsync(@Path("user") String user, Callback<List<Repo>> callback);

        @GET("/repos/{user}/{repo}/contributors")
        void listContributorsAsync(@Path("user") String user, @Path("repo") String repo,
                                   Callback<List<Contributor>> callback);
    }
}
