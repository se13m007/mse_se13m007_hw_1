package at.wien.technikum.holzer.se13m007_helloworld;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Holzer on 25.11.2014.
 * MSE 3 B
 */
public class RepoFragment extends Fragment {
    private String username;
    private ProgressDialog progressDialog;
    private ListView listView;

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * is being called when the fragment is created
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // create the progressDialog here, to avoid to create it every time something is clicked
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading Repos ...");

        // get the visual elements -> view, button, input-textfield, output-textview
        View rootView = inflater.inflate(R.layout.list_main, container, false);
        listView = (ListView) rootView.findViewById(R.id.listView);
        showRepos(username);
        return rootView;
    }

    /**
     * shows all repos of the provided user
     */
    public void showRepos(final String user) {
        // show the progressDialog
        progressDialog.show();
        // make a RetrofitAPI-call to get all repos from the user
        RetrofitApiClient.getInstance().getReposForUser(user, new Callback<List<Repo>>() {
            /**
             * will be called if there is a failure (i.e. if user does not exist)
             */
            @Override
            public void failure(RetrofitError error) {
                if(error == null || error.getResponse() == null) {
                    showFailureMessage("Bitte Internetverbindung prüfen");
                }
                else if(error.getResponse().getStatus() == 404)
                    showFailureMessage("User not found");
                else {
                    showFailureMessage("Retrofit Error!");
                }
            }

            /**
             * will be called if the RetrofitAPI-call was successful
             */
            @Override
            public void success(final List<Repo> repoList, Response response) {
                // close the progress dialog
                progressDialog.hide();

                RepoAdapter adapter = new RepoAdapter(getActivity(), repoList);
                // Attach the adapter to a ListView
                listView.setAdapter(adapter);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        ContributorFragment contributorFragment = new ContributorFragment();
                        contributorFragment.setUsername(user);
                        contributorFragment.setRepo(((Repo)listView.getItemAtPosition(position)).name);
                        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.container, contributorFragment);
                        fragmentTransaction.addToBackStack(null).commit();
                    }
                });
            }
        });
    }

    public void showFailureMessage(String message) {
        // close the progress dialog
        progressDialog.hide();

        // show failure message in output-textview and on dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setTitle(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
