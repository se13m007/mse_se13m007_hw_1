package at.wien.technikum.holzer.se13m007_helloworld;

/**
 * Created by Holzer on 04.12.2014.
 * MSE 3 B
 */
public class Contributor {
    public int id;
    public String login;
    public int contributions;

    public Contributor(int id, String login, int contributions) {
        this.id = id;
        this.login = login;
        this.contributions = contributions;
    }

    @Override
    public String toString() {
        return "Contributor: [id=" + id + ", login=" + login + ", contributions=" + contributions + "]";
    }
}
